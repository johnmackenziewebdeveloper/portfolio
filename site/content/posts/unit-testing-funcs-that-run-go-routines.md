---
title: "Unit Testing Funcs That Run Go Routines"
date: 2020-11-30T14:17:28Z
post_date: 30 November 2020
draft: true
---
Whenever possible I always try and work in a TDD paradigm. It is important to me that I have not only decent coverage,
but that I write the tests as I develop. I have found that by doing this that my solutions are logically much
clearer.

```go
// Run begins a background process
// This go routine can closed down by cancelling the supplied context
func (p Provider) Run(ctx context.Context) chan error {
	errCh := make(chan error)
	// Run the kafka implementation. view.Run will run until either the context is cancelled (the error will be nil)
	// or an error is encountered.
	go func() {
        // PS: always check that the error is not nil
		if err := p.view.Run(ctx); err != nil {
			errCh <- err
		}
        // close the channel created in this func; It is important that
        // the producer closes the channel.
		close(errCh)
	}()
	return errCh
}
```

{{% googlead %}}

The unit test for this function looked like;
```go
//go:generate mockery --name Runner
// TestProvider_Run since provider.Run() spins up a go routine inside itself. It is important that we
// test to ensure when the context is cancelled, everything is clean up as expected
func TestProvider_Run(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	mockRunner := mocks.Runner{}
	mockRunner.On("Run", ctx).Return(nil)

	sut := Provider{
		view: &mockRunner,
	}

	// get the number of running go routines. We will expect this to be the same after the test has ran
	expectedNumGoroutines := runtime.NumGoroutine()

	// Run runs a go routine inside in itself
	outCh := sut.Run(ctx)

	// cancel the supplied context, this should ensure channel is closed and go routine exits
	cancel()

	select {
	// this is a catch, to stop the test infinite looping
	case <-time.After(1 * time.Second):
		assert.FailNow(t, "test timed out, assumed infinite loop")
	case err, ok := <-outCh:
		// assert the returned error channel is closed
		assert.False(t, ok)
		// assert the error returned is nil
		assert.Nil(t, err)
	}
	assert.Equal(t, expectedNumGoroutines, runtime.NumGoroutine())
	mockRunner.AssertExpectations(t)
}
```

## Things to Note
1. Mocking is a god send. For almost all my mocking I use[mockery](https://github.com/vektra/mockery).
Mockery takes an interface and creates a concrete type who's return values can be completely configured by us.
2. Watch those go routines! Before I `act` on my test I set the expected number of go routines. When I have cancelled 
the context I expect the go routine that was spun up to close also.
`runtime.NumGoroutine()` will return the number of go routines running at the time this func is called.
I will check after the method has returned that the number of go routines is the same as when I started.
I will assert that once I have cancelled the context I want to make sure the go routines that
I span up from within the `Run` method.
3. Watch out for the infinite loop. When working with a test that implements channels, I always make sure that
I have a select statement and at least one case that waits on a time.After.
Time after sends on the returned channel after the configured time. So if I do have a bug in my channel code.
This test will `timeout` after 1 second.
4. For assertions I use [Testify](https://github.com/stretchr/testify).
I enjoy the concise readability that its API offers.