---
title: "Fulfilling the Read Closer Interface"
date: 2020-11-30T15:14:05Z
post_date: 30 November 2020
---

I often forget how to fulfill the io.ReadCloser interface.
The below snippet creates a ReadCloser who's contents equal "Hello World!"

```go
func main() {
	reader := ioutil.NopCloser(strings.NewReader("Hello World!"))
	GiveMeAReader(reader)
}

func GiveMeAReader(r io.ReadCloser) {}
```

{{% googlead %}}