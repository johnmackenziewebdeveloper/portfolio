---
title: "TIL MySQL Assumes an missing value to be empty string"
date: 2021-07-20T10:00:31Z
post_date: 20July 2021
excerpt: Investigating null behaviour in mysql
description: Investigating null behaviour in mysql
---

Today I learned that when mysql is NOT running in strict mode. Missing values to `NOT NULL` columnes are assumed to be an empty string. Allow me to demonstrate.

In the above example baz we have specified that values in the baz column must not be null

```
CREATE TABLE foo (
	bar VARCHAR(1),
	baz VARCHAR(1) NOT NULL
);

# Works hooray
INSERT INTO foo (bar, baz) VALUES ("value_1", "value_2");

# This does NOT work because baz == NULL and this is expected.
INSERT INTO foo (bar, baz) VALUES ("value_1", NULL);

# baz is missing from insert. However because it is not explicitely set as NULL mysql assumes that we want an empty string
INSERT INTO foo (bar) VALUES ("value_1");
```

I then found out which mode I had mysql running in;

```
SHOW VARIABLES LIKE 'sql_mode';
```

```
Variable_name	Value
sql_mode	    NO_ENGINE_SUBSTITUTION
```

> NO_ENGINE_SUBSTITUTION is the default mode.

{{% googlead %}} 

I want to set sql_mode to STRICT_TRANS_TABLES as per the [documentation](https://dev.mysql.com/doc/refman/5.6/en/sql-mode.html#sql-mode-strict).

```
set global sql_mode='STRICT_TRANS_TABLES';
```

> PS. If you are using Sequel Pro you may need to login/logout.

Now running;

```
INSERT INTO foo (bar) VALUES ("value_1");	
```

```
Field 'baz' doesn't have a default value
```

I can't believe that I have worked with mysql for so long and not come across this before.



