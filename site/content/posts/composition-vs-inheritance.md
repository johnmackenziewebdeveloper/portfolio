---
title: Composition Vs Inheritance
author: John Mackenzie
date: 2021-03-15
draft: true
post_date: 15th March 2021
excerpt: What are they key difference and when should I choose on over the other?
description: What are they key difference and when should I choose on over the other?
---

> I will be using the world `class` to mean class, struct, object or any king of data thing that encapsulates functionality.

Inheritance aquires functionality from its parent class usually via the `extends` keyword. This method is modelled off genealogy. Think real life parents and the charactaristics that are inherited by their child.



In Composition a class aquires its functionality from the classes that it is composed of. Think frankensteing monster. Think an arm stitched on that has a special ability to catch, legs from a class that is particularly good at running. The torso of Arnold Swartznegger. You get the picture.

On the face of it inheritance data model is tightly coupled. Changing functionality in the parent class, could alter will alter the functionlity in the child.

On the other hand composition is loosly coupled as parts can be chopped and changed with less chance of breaking the code. As long as the new part has all the functionlaity as the last. You class will continue to have the same methods it has always had.



