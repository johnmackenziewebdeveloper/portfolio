---
title: "From mysql To Kafka using Kafka Connect"
date: 2020-05-27
post_date: 27 May 2020
draft: true
---

## The Project

I am going to be using kafka connect to stream changes made to a mysql sql db into a kafka cluster. we will be using  pgcli to manipulate the mysql database, and kafkacat to read the messages on the kafka stream on the other side.

This project is going to be developed locally using docker-compose. All of our components will be declared in our docker-compose.yaml file. See below a diagram of the moving parts and how this fit together.

[image here]

## Infrastructure

First off we are going to set up our infrastructure, we will get all the docker containers built and speaking to each other.

### Step One: Zookeeper & Kafka

You have probably seen this peice a thousand times before, so I will be brief. We are gong to bring up a zookeeper instance which will be used to help orchestrate our lonesome docker broker.

```
    connect-tut-zookeeper:
        image: confluentinc/cp-zookeeper:3.2.0
        ports:
            - 2181:2181
        environment:
            - ZOOKEEPER_CLIENT_PORT=2181

    connect-tut-kafka:
        image: confluentinc/cp-kafka
        ports:
        - 9092:9092
        environment:
            - KAFKA_ZOOKEEPER_CONNECT=connect-tut-zookeeper:2181
            - KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR=1
            - KAFKA_ADVERTISED_LISTENERS=PLAINTEXT://connect-tut-kafka:9092
        depends_on:
            - connect-tut-zookeeper
```

One thing worth noting for me is the env var `KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR` this helped me to overcome the `Exception: replication factor: 3 larger than available brokers: 1` error.

{{% googlead %}} 

### Step Two: The Schema Registry

The Schema registry is a REST api which will hold our avro schemas. 

```
connect-tut-schema-registry:
    image: confluentinc/cp-schema-registry:3.2.0
    ports:
        - 8081:8081
    environment:
        - SCHEMA_REGISTRY_HOST_NAME=schema-registry
        - SCHEMA_REGISTRY_KAFKASTORE_CONNECTION_URL=connect-tut-zookeeper:2181
        - SCHEMA_REGISTRY_LISTENERS=http://connect-tut-schema-registry:8081
    depends_on:
      - connect-tut-kafka
```

`SCHEMA_REGISTRY_LISTENERS` tells us the domain and port we will need to use to access our schemas. This will be used by the exports (see below). The exporter will automaticalls add a schema upon configuration.

### Step Three: The DB

A mysql database instance. This contain relies on nothing as it is the starting point of our data pipeline.

```
connect-tut-db:
    container_name: kafka-connect-tut-db
    image: mysql:5.7
    ports:
        - 3306:3306
    volumes:
        - ./mysql/init.sql:/docker-entrypoint-initdb.d/init.sql
    environment:
        - MYSQL_ROOT_PASSWORD=confluent
        - MYSQL_USER=confluent
        - MYSQL_PASSWORD=confluent
        - MYSQL_DATABASE=connect_test
```

We will volume mount an SQL script that will init our schema. See below;

```
CREATE DATABASE IF NOT EXISTS connect_test;
USE connect_test;

DROP TABLE IF EXISTS test;

CREATE TABLE IF NOT EXISTS test (
    id serial NOT NULL PRIMARY KEY,
    name varchar(100),
    email varchar(200),
    department varchar(200),
    modified timestamp default CURRENT_TIMESTAMP NOT NULL,
    INDEX `modified_index` (`modified`)
);

INSERT INTO test (name, email, department) VALUES ('alice', 'alice@abc.com', 'engineering');
```

These are the values that are going to be read into Kafka and eventually consumed by our golang consumer service.

### Step Four: The Connector

Finally the headline act, the connector. The connector will watch mysql's write ahead log and produce onto our kafka broker whenever anything changes.

```
connect-tut-avro:
    container_name: kafka-connect-avro
    image: confluentinc/cp-kafka-connect:latest
    ports:
        - 8082:8082
    volumes:
        - /tmp/quickstart/file:/tmp/quickstart
        - /tmp/quickstart/jars:/etc/kafka-connect/jars
    environment:
        - CONNECT_BOOTSTRAP_SERVERS=connect-tut-kafka:9092
        - CONNECT_REST_PORT=8082
        - CONNECT_GROUP_ID=quickstart-avro
        - CONNECT_CONFIG_STORAGE_TOPIC=quickstart-avro-config
        - CONNECT_OFFSET_STORAGE_TOPIC=quickstart-avro-offsets
        - CONNECT_STATUS_STORAGE_TOPIC=quickstart-avro-status
        - CONNECT_CONFIG_STORAGE_REPLICATION_FACTOR=1
        - CONNECT_OFFSET_STORAGE_REPLICATION_FACTOR=1
        - CONNECT_STATUS_STORAGE_REPLICATION_FACTOR=1
        - CONNECT_KEY_CONVERTER=io.confluent.connect.avro.AvroConverter
        - CONNECT_VALUE_CONVERTER=io.confluent.connect.avro.AvroConverter
        - CONNECT_KEY_CONVERTER_SCHEMA_REGISTRY_URL=http://connect-tut-schema-registry:8081
        - CONNECT_VALUE_CONVERTER_SCHEMA_REGISTRY_URL=http://connect-tut-schema-registry:8081
        - CONNECT_INTERNAL_KEY_CONVERTER=org.apache.kafka.connect.json.JsonConverter
        - CONNECT_INTERNAL_VALUE_CONVERTER=org.apache.kafka.connect.json.JsonConverter
        - CONNECT_REST_ADVERTISED_HOST_NAME=kafka-connect-avro
        - CONNECT_LOG4J_ROOT_LOGLEVEL=INFO
        - CONNECT_PLUGIN_PATH=/usr/share/java,/etc/kafka-connect/jars
    depends_on:
        - connect-tut-kafka
        - connect-tut-schema-registry
        - connect-tut-db
```

As you can see there is quite a lot of configuration that is needed here. Take your time and read it carefully. At a high level view this image depends on the db (to read from), the schema registry (to learn how to encode the message) and kafka (to write to).

## Let's Do Stuff

First of all we are going to begin watching our kafka broker to see our db changes propagate through. To do this we will run a docker image that has kafkacat installed and add it to our network.

```
docker run --tty \
	--network kafka-connect_default \
	confluentinc/cp-kafkacat \
	kafkacat -b connect-tut-kafka:9092 \
	-t quickstart-jdbc-test
```

Finally we will use a mysql client to add some records to the mysql db. This again will be done via a docker image to save us to from installing pgcli locally.

```
docker run -it \
    --network kafka-connect_default \
    --rm \
    mysql mysql -hconnect-tut-db -uconfluent -pconfluent -Dconnect_test
```

Then run some inserts;

```alittle
INSERT INTO test (name, email, department) VALUES ("john", "john@example.com", "development");
```


These changes will then begin to propagate through into kafka, and should begin to appear in the terminal running kafkacat


