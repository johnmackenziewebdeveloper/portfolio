---
title: My most useful bash aliases and functions
author: John Mackenzie
date: 2018-12-12
post_date: 16th December 2018
excerpt: Today I thought I would share the contents of my .zshrc file. I hope this post will be useful to someone.
description: Today I thought I would share the contents of my .zshrc file. I hope this post will be useful to someone.
image: /images/terminal.jpg
draft: true
---

I am big on efficiency. I am always on the look out for useful and time saving aliases/functions that can improve my
productivity and make me a better developer. So here is a sneak peek into my .zshrc, I hope it provides some use to someone.
I will split them up into the key areas I work with;

### Docker

Every get tired of typing docker-compose? Save yourself 12 key strokes each time and go for `dc`.

```
alias dc="docker-compose"
```

I use docker day in day out. Building and tearing down machines continuously. Whenever I need to turn all off quickly
because lets say I have a clash on a port or I do not have my charger with me and need to save energy, I can run `docker-stop-all`.

I should be so comfortable with my projects that I can constantly tear them down and rebuild them back up. It also keeps my HDD clear.
```
function docker-stop-all {
  docker stop $(docker ps -aq)
}

alias docker-nuke="docker rm $(docker ps -a -q) && docker rmi $(docker images -q) -f"
```

### Golang

Here is a useful alais to run all my golang unit tests recursively. Before I commit and push my work I run;

create-go-proj is more out of laziness than efficiency. This essentially gives me a basic skeleton of a typical go project.
```
function create-go-proj {
  echo "Creating new Go project called ${1}"
  mkdir $1
  cd $1
  touch "main.go"
  touch "main_test.go"
  echo "package main" > "main.go"
  echo "package main" > "main_test.go"
  echo "${1} successfully created!"
}
```

### Git

These have been done to death but common helpful git shortcuts, everything from push, to pulling to catching typos;
```
alias gst="git status"
alias gco="git fetch && git checkout"
alias gp="git push"
alias diff="git diff"
alias git dif="git diff" // Spell Check
```

### PHP

Granted my PHP section is looking rather lacking barren, I will be updating this at a later date.
```
# PHP
alias pa="php artisan"
```


### Terraform

I really hate typing terraform workspace select/new, and I keep thinking that terraform has a create method. So it is good to catch that also.
```
# Terraform
alias twn="terraform workspace new"
alias twc="terraform workspace new"
alias tws="terraform workspace select"
```