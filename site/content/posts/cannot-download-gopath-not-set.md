---
title: Cannot Download, $GOPATH not set
author: John Mackenzie
date: 2017-04-19
post_date: 17th December 2017
excerpt: How to avoid this error when adding external packages
description: How to avoid this error when adding external packages
image: /images/go-gopher.png
---

## What is wrong here?
This error occurs when you have not set your go home location, because you are downloading an external package go will need to save the file somewhere and you need to tell it where you are working.

On the commandline;

```sybase
echo $GOPATH
```

To confirm that that is in fact the problem. If it isn't I can't help you :(

Then on the command line type;

```sybase
export GOPATH=/link/to/your/current/directory
```

Or if you want to be clever add it as an alias to your ~/.bashrc

```sybase
alias gohome="export GOPATH=$(pwd)"
```

Now from the command line all you have to type is;

```sybase
gohome
```

GOPATH will be get set to your current directory.