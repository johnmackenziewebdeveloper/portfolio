---
title: PHP Call Magic Method
author: John Mackenzie
date: 2017-04-19
post_date: 4th October 2017
excerpt: The _call() magic method. what is it? and when do we use it?
description: The _call() magic method. what is it? and when do we use it?
image: /images/magicians-hat.jpg
---

## What is does?
If defined this method is will be ran if a method that does not exist is called.

## Why would you use it?
You would use it to gracefully catch calling an undefined function call, Laravel uses this very well with its getting and setting on attributes and relationships. To see more about this please read here.

### Example

```php
class User {

    public function __call($method, $args) {

        echo "unknown method " . $method;

        return false;

    }

}

$user = new User();

$user->aFunctionThatDoesNotExist(); //returns an echo keeping your code from erroring

```

