---
title: "A Few Notes on Go Versioning"
date: 2020-06-01T08:07:58+01:00
post_date: 01 June 2020
draft: false
---

Go follows semantic versioning, and on top of that makes a few assumptions based on which version you are on.

## Semantic Versioning (SemVer) Refresh

Semantic Versioning looks like `MAJOR.MINOR.PATCH`. Eg `1.10.8` , or ` 2.12.8`

* Major patch signals a breaking change. For example if you have a package on 1.2.3 and you  upgrade to 2.0.0. This signals that the API has changed from major version 1 => 2. You will have to update the way that you are using the package. Possible examples are method name changes, deprecations and parameter changes.

* Minor change signals that features have been added and that there will be no breaking changes to your codebase because of these. Possible examples are additional methods added , additional "optional" parameters to existing functions.

* Patch is used whe you make backwards compatable bug fixes.

{{% googlead %}}

Final: If any change is made which means tha you users are going to have to change exisitng usage of your package, you MUST increment a major.

## How this relates to Golang

Setting the scene, assume that we have a package called `github.com/someguy/coolpkg/v2` with the following versions;

```
v2.1.0
- Added func CountToTen
v2.0.0
- updated func Getname() -> GetName()
v1.7.1
- Something added
```



### Version v1 and earlier...

When go getting a package version =< 1 (and yes I mean all the way upto 1.9999.9999999) then go mod will assume that the package is under heavy development and thus the API will be prone to change. It will make no assumptions re backwards compatibility.

#### Installation

When using the `go get github.com/someguy/coolpkg` the latest minor + patch of v1 will be installed. In the case of our example `v1.7.1` will be installed.

### Version v2 and after

Version 2 and greater will assume backwards compatibility has been broken when the major version has been incremented. When using a package on >= v2 the version MUST be put onto the end of the import statement.

`import github.com/someguy/coolpkg/v2`

This library is still accessible in your code as coolpkg.SomeMethod() but the version is necessary. This is to avoid the [Diamond Dependency Problem](https://www.well-typed.com/blog/2008/04/the-dreaded-diamond-dependency-problem/)

#### Installation

As mentioned above `go get` will install the last minor + patch of v1. In the case of our example `v1.7.1` will be installed. If you wish to install v2 of a package we will have to specify that and run `go get github.com/someguy/coolpkg/v2`.

<!--adsense-->

### Getting a specific version

Similar to other package management systems go get can take queries for package versions. These can be any of the following possibilities;

* Specific version `go get github.com/someguy/coolpkg@v2.0.0`
* Version prefix  `go get github.com/someguy/coolpkg@v2`
* Latest                `go get github.com/someguy/coolpkg@latest`
* Specific Commit `go get github.com/someguy/coolpkg@d295628`
* Specific Branch `go get github.com/someguy/coolpkg@master`
* Comparison `go get github.com/someguy/coolpkg@>=2.1.0`

### Finally... Don't forget the v

In go all versions are prefixed with a "v". See `v1.2.3`, `v8.0.0`.