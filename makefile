AWS_PROFILE=john-static-site
BUCKET_NAME=www.johnmackenzie.co.uk
DOMAIN_LIVE=https://www.johnmackenzie.co.uk/

terraform-plan:
	cd ./terraform && \
	terraform plan \
		-var 'aws_profile=${AWS_PROFILE}' \
		-var 'website_bucket_name=${BUCKET_NAME}'

terraform-run:
	cd ./terraform && \
	terraform apply \
		-var 'aws_profile=${AWS_PROFILE}' \
		-var 'website_bucket_name=${BUCKET_NAME}'

terraform-destroy:
	cd ./terraform && \
	terraform destroy \
		-var 'aws_profile=${AWS_PROFILE}' \
		-var 'website_bucket_name=${BUCKET_NAME}'

site-upload:
	aws --profile="${AWS_PROFILE}" s3 sync ./site/public s3://$(BUCKET_NAME)

site-build:
	cd ./site && \
	hugo --theme=hugo_theme_pickles --baseURL=${DOMAIN_LIVE}

up:
	cd ./site && \
	hugo serve -t hugo_theme_pickles -D

# .PHONY upload
# upload:
#   Does not work One.com is crap
#     rsync -a ./public johnmackenzie.co.uk@ssh.johnmackenzie.co.uk:/customers/8/b/a/johnmackenzie.co.uk/httpd.www