variable "aws_region" {
  default = "us-east-1"
}

variable "aws_profile" {}

variable "domain_name" {
  default = "www.johnmackenzie.co.uk"
}

variable "website_bucket_name" {}